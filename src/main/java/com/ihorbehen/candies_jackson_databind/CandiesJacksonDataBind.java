package com.ihorbehen.candies_jackson_databind;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ihorbehen.Model.Candies;
import com.ihorbehen.Model.Candy;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class CandiesJacksonDataBind {

    public static void parseJson(String path) {
            ObjectMapper objectMapper = new ObjectMapper();
        Candies candies = null;
        try {
            candies = objectMapper.readValue(new File(path), Candies.class);
            List<Candy> candyList = (candies.getCandy());
            candyList.sort(new Candy.SortByCandyId());
            candyList.forEach(System.out::println);

        } catch (IOException e) {
            e.printStackTrace();
        }
//        for (Candy c : candies.getCandy()) {
//            System.out.println(c);
//        }
    }
}
