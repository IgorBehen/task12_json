package com.ihorbehen.candies_gson;

import com.ihorbehen.candie_validation.CandieValidation;
import com.ihorbehen.candies_gson.candies_gson_parser.CandiesGsonParser;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        if(CandieValidation.validateJson("E:\\Igor\\EPAM_JAVA\\my_repos\\TestIdea\\target\\jsonschema.json", "candy.json"))
            CandiesGsonParser.parseJson("E:\\Igor\\EPAM_JAVA\\my_repos\\task12_JSON\\candy.json");
    }
}
