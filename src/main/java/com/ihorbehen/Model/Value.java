package com.ihorbehen.Model;

public class Value {
    private CandyGroup candyGroup;
    private int proteins;
    private int fats;
    private int carbohydrates;

    public Value(CandyGroup candyGroup, int proteins, int fats, int carbohydrates) {
        this.candyGroup = candyGroup;
        this.proteins = proteins;
        this.fats = fats;
        this.carbohydrates = carbohydrates;
    }

    public Value() {

    }

    public CandyGroup getCandyGroup() {
        return candyGroup;
    }

    public void setCandyGroup(CandyGroup candyGroup) {
        this.candyGroup = candyGroup;
    }

    public double getProteins() {
        return proteins;
    }

    public void setProteins(int proteins) {
        this.proteins = proteins;
    }

    public double getFats() {
        return fats;
    }

    public void setFats(int fats) {
        this.fats = fats;
    }

    public double getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(int carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    @Override
    public String toString() {
        return "Type{" +
                "candyGroup=" + candyGroup.toString() +
                "proteins=" + proteins +
                ", fats=" + fats +
                ", carbohydrates=" + carbohydrates +
                '}';
    }
}
