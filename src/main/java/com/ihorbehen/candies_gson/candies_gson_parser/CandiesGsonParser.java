package com.ihorbehen.candies_gson.candies_gson_parser;

import com.google.gson.Gson;
import com.ihorbehen.candies_gson.model.Candies;
import com.ihorbehen.candies_gson.model.Candy;

import java.io.FileReader;
import java.io.IOException;

public class CandiesGsonParser {

    public static void parseJson(String path) {
        Gson gson = new Gson();
        Candies candies = null;
        try {
            candies = gson.fromJson(new FileReader(path), Candies.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Candy c : candies.getCandy()) {
            System.out.println(c);
        }
    }
}