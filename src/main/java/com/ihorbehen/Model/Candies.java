package com.ihorbehen.Model;

import java.util.List;

public class Candies {
    private List<Candy> candy;

    public List<Candy> getCandy() {
        return candy;
    }

    public void setCandy(List<Candy> candy) {
        this.candy = candy;
    }
}
