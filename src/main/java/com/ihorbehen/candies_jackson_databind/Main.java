package com.ihorbehen.candies_jackson_databind;

import com.ihorbehen.candie_validation.CandieValidation;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        if(CandieValidation.validateJson("jsonschema.json", "candy.json"))
        CandiesJacksonDataBind.parseJson("E:\\Igor\\EPAM_JAVA\\my_repos\\task12_JSON\\candy.json");
    }
}
