package com.ihorbehen.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class Candy {
  private int candyId;
  private String name;
  private String production;
  private Value value;
  private Type type;
  private int energy;
  private List<Ingredient> ingredients = new ArrayList<>();

  public Candy(int candyId, String name, String production, Value value, Type type, int energy, List<Ingredient> ingredients) {
    this.candyId = candyId;
    this.name = name;
    this.production = production;
    this.value = value;
    this.type = type;
    this.energy = energy;
    this.ingredients = ingredients;
  }

  public Candy() {

  }

  public int getCandyId() {
    return candyId;
  }

  public void setCandyId(int candyId) {
    this.candyId = candyId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
  public String getProduction() {
    return production;
  }

  public void setProduction(String production) {
    this.production = production;
  }

  public Value getValue() {
    return value;
  }

  public void setValue(Value value) {
    this.value = value;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public int getEnergy() {
    return energy;
  }

  public void setEnergy(int energy) {
    this.energy = energy;
  }
  public List<Ingredient> getIngredients() {
    return ingredients;
  }

  public void setIngredients(List<Ingredient> ingredients) {
    this.ingredients = ingredients;
  }

  public static class SortByCandyId implements Comparator<Candy> {

    @Override
    public int compare(Candy o1, Candy o2){
      return Integer.compare(o1.getCandyId(), o2.getCandyId());
    }
  }

  @Override
  public String toString() {
    return "Candie{" +
        "candyId=" + candyId +
        ", name='" + name + '\'' +
        ", production='" + production + '\'' +
        ", value=" + value +
        ", type=" + type +
        ", energy=" + energy +
        ", ingredients=" + ingredients +
        "}\n";
  }


}
