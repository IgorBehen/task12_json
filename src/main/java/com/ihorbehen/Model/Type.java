package com.ihorbehen.Model;

public class Type {
  private CandyGroup candyGroup;
  private boolean caramel;
  private boolean iris;
  private boolean chocolate;
  private boolean isStuffed;

  public Type(CandyGroup candyGroup, boolean caramel, boolean iris, boolean chocolate, boolean isStuffed) {
    this.candyGroup = candyGroup;
    this.caramel = caramel;
    this.iris = iris;
    this.chocolate = chocolate;
    this.isStuffed = isStuffed;
  }

  public Type() {

  }

  public CandyGroup getCandyGroup() {
    return candyGroup;
  }

  public void setCandyGroup(CandyGroup candyGroup) {
    this.candyGroup = candyGroup;
  }

  public boolean isCaramel() {
    return caramel;
  }

  public void setCaramel(boolean caramel) {
    this.caramel = caramel;
  }

  public boolean isIris() {
    return iris;
  }

  public void setIris(boolean iris) {
    this.iris = iris;
  }

  public boolean isChocolate() {
    return chocolate;
  }

  public void setChocolate(boolean chocolate) {
    this.chocolate = chocolate;
  }

  public boolean getIsStuffed() {
    return isStuffed;
  }

  public void setStuffed(boolean isStuffed) {
    this.isStuffed = isStuffed;
  }

  @Override
  public String toString() {
    return "Type{" +
        "candyGroup=" + candyGroup.toString() +
        ", caramel=" + caramel +
        ", iris=" + iris+
        ", cooler=" + chocolate +
        ", isStuffed=" + isStuffed +
        '}';
  }
}
